// An example module
package module

import (
	"encoding/json"
	"net/http"
	"github.com/gorilla/mux"
	. "gitlab.com/sporkboy/kdebugmode/debugserver"
)

var thisModule = KDebugModule{
	Namespace:	"example",
	Handlers:	[]Handler{},
}

var (
	endpoints = make(map[string]func(w http.ResponseWriter, r *http.Request))
	varsToReturn = make(map[string]string)
)

func init() {
	endpoints[thisModule.Namespace] = GetValue
	endpoints[thisModule.Namespace + "/{id}"] = GetValues

	varsToReturn["hw"] = "Hello World"
	varsToReturn["why"] = "Why did the chicken cross the road?"
	varsToReturn["spoon"] = ""
	varsToReturn["who"] = "There is no Dana, only ZUUL!"

	// Populate the handlers we will respond to
	addHandler(Handler{
		Path:			"value",
		HandlerFunc: 	GetValues,
		Method:	 		"GET",
	})
	addHandler(Handler{
		Path:			"value/{id}",
		HandlerFunc: 	GetValue,
		Method:	 		"GET",
	})
	addHandler(Handler{
		Path:			"value/{id}?value={value}",
		HandlerFunc: 	GetValue,
		Method:	 		"PUT",
	})
}

// We need to do this:
//router.HandleFunc("/os/env", GetValues).Methods("GET")
//router.HandleFunc("/os/env/{id}", GetValues).Methods("GET")

// This method is called by your code when you enable this module.
func RegisterMe() (KDebugModule) {
	return thisModule
}

// Add a handler to the list of what this module serves.
func addHandler(handler Handler) {
	thisModule.Handlers = append(thisModule.Handlers, handler)
}

// ########################################################################################
// These functions handle HTTP requests
// ########################################################################################

func GetValues(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(getVars())
}

func GetValue(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, _ := params["id"]

	json.NewEncoder(w).Encode(getVar(id))
}

func SetValue(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, _ := params["id"]
	val, _ := params["value"]

	json.NewEncoder(w).Encode(addVar(id, val))
}

// ########################################################################################
// These functions do the actual work. A little redundant in this simple example.
// ########################################################################################

// Return an array of all environment variables
func getVars() (map[string]string) {
	return varsToReturn
}

// Return a specific environment variable.
func getVar(id string) (string) {
	return varsToReturn[id]
}

func addVar(key string, value string) (map[string]string) {
	varsToReturn[key] = value
	return varsToReturn
}