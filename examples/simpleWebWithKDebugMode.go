package main

import (
	"github.com/gorilla/mux"
	"github.com/golang/glog"
	"net/http"
	"encoding/json"
	"time"

	kdb "gitlab.com/sporkboy/kdebugmode/debugserver"
	kdbo "gitlab.com/sporkboy/kdebugmode/os"
	//kdbe "gitlab.com/sporkboy/kdebugmode/examples/module"
)

func Hello(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode("Hello World")
}

func main() {
	router := mux.NewRouter()

	listenAddr := ":8080"

	// Expose application functionality
	router.HandleFunc("/", Hello).Methods("GET")

	// Register the OS module and start the kdebugmode server.
	kdb.RegisterModule(kdbo.RegisterMe())
	kdb.ServeDebug()

	glog.Infof("Application startup on port %d and redir port %d at: %s\n", listenAddr, time.Now())
	glog.Fatal(http.ListenAndServe(listenAddr, router))
}
