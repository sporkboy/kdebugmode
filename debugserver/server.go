package debugserver

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"time"

	"github.com/golang/glog"
	"github.com/gorilla/mux"
)

// I don't think I can use an actual interface since I'm not implementing modules as types but
// each module has to implement the RegisterModule() ([]string, []func()) {} function.

//TODO: Rename this package to something like base
//TODO: We need to return more than endpoints. Each endpoint needs to tell us it's method. We may also need it to provide some sort of documentation in order to make swagger happen.
//TODO: KDebugModule should also have a boolean for writable which disables set methods. This should be disabled by default? Do we want each handler to carry that flag?

// A KDebugModule offers any number of methods which can be used to debug containers running in an orchestration environment such as Kubernetes.
type KDebugModule struct {
	Namespace		string
	Handlers 		[]Handler
}

// A Handler is how modules tell us of a function and how to call it. Where Path is the sub path for the request which
// will be added to the end of their base path (it should not start with a /). HandlerFunc is a reference to the
// function to call when recieving requests. And method is the HTTP verb this handler will respond for.
// Acceptable verbs are needs to be one of these https://golang.org/src/net/http/method.go.
type Handler struct {
	Path			string
	HandlerFunc 	func(w http.ResponseWriter, r *http.Request)
	Method	 		string // Actually needs to be one of these https://golang.org/src/net/http/method.go but I don't see how to require that in go
	//Documentation 	string // Docs for this method?
}

var (
	// This is multiple values because we are going to allow users to set the first and the second will be set from a build variable.
	basePath 		= "/api"
	version 		= "0.1"
	base 			= basePath + "/" + version + "/"

	router 			*mux.Router

	//TODO: Pull this from an environment variable or default.
	kdebugAPIPort 	= 8435

	modules 		= []KDebugModule{}
)


// Built in self-signed certificate and key
var keyPem = `-----BEGIN RSA PRIVATE KEY-----
MIIBOgIBAAJBAMRsCahN8Kd4UjaKsLz5fJbY33szas/hRgLTpesXtWfsPMbiVwAT
Io53W/po9JxlFT60zgW2cDpnjYvhEG6213ECAwEAAQJAQJpUFtIDq+EsERpOhOj9
rFOKNWg04khykW1xLcBvfs0k/OitzUtZmTszBMecSG8+bUIusR89i3iTjDtOl0iq
wQIhAPdUT2ZW8XOyozs2m46/Jk2Y6hl+mcuoKv+ix533vm/ZAiEAy07aZ60k4Q3d
+4s6BT9jHK2WZrh4yRJJ3iauZ83tfVkCIC103Gb1wci+40tFvQ9eSF7opNXss71/
ee+j7kY1O1X5AiEAsHb880hB3mXe61rpPWzLZjT1eqtbi4soOn5wmwEOxeECIDV7
4zSxt4B1QolrYjybjbp0KW5wve7pli3Wd7yRr5qM
-----END RSA PRIVATE KEY-----`

var certPem = `-----BEGIN CERTIFICATE-----
MIIBxjCCAXACCQDwxBbNDS5h5jANBgkqhkiG9w0BAQUFADBqMQswCQYDVQQGEwJV
UzETMBEGA1UECBMKV2FzaGluZ3RvbjEQMA4GA1UEBxMHU2VhdHRsZTEUMBIGA1UE
ChMLVGhlIFViZXJsYWIxHjAcBgNVBAMTFWxvY2FsaG9zdC5sb2NhbGRvbWFpbjAe
Fw0xODA0MDYxODA1MzhaFw0xOTA0MDYxODA1MzhaMGoxCzAJBgNVBAYTAlVTMRMw
EQYDVQQIEwpXYXNoaW5ndG9uMRAwDgYDVQQHEwdTZWF0dGxlMRQwEgYDVQQKEwtU
aGUgVWJlcmxhYjEeMBwGA1UEAxMVbG9jYWxob3N0LmxvY2FsZG9tYWluMFwwDQYJ
KoZIhvcNAQEBBQADSwAwSAJBAMRsCahN8Kd4UjaKsLz5fJbY33szas/hRgLTpesX
tWfsPMbiVwATIo53W/po9JxlFT60zgW2cDpnjYvhEG6213ECAwEAATANBgkqhkiG
9w0BAQUFAANBAIpo8bBP8XA6z2Gmabwrb1upTLQej7eryDYFPpfr0h+Exo5CaMcV
yJY7FINYvIVBVbCANkEFA8RmOstlL1zzgZc=
-----END CERTIFICATE-----`

// Call this in your code to add each module you want to use.
func RegisterModule(module KDebugModule) {
	modules = append(modules, module)

	//// Let's experiment with this later on the off chance that we can register after we have called listenand serve.
	//moduleNS := base + module.Namespace
	//for _, value := range module.Handlers {
	//	router.HandleFunc(moduleNS + "/" + value.Path, value.HandlerFunc).Methods(value.Method)
	//}
}

// Register all handlers with the router
func registerModules(router *mux.Router) {
	for _, value := range modules {
		moduleBase := base + value.Namespace
		for _, value2 := range value.Handlers {
			router.HandleFunc(moduleBase + "/" + value2.Path, value2.HandlerFunc).Methods(value2.Method)
		}
	}
}

// Change the port which kdebugmode will listen on.
func SetListenPort(port int) {
	kdebugAPIPort = port
}

//TODO: Validate these and return errors
// Change the certificate and matching key which is used to encode traffic.
func SetKeyCertPem(key string, cert string) error {
	keyPem = key
	certPem = cert
	return nil
}

// main function to boot up everything
func ServeDebug() {

	router := mux.NewRouter()

	// Expose application functionality
	registerModules(router)


	//router.HandleFunc("/os/env", GetEnvirons).Methods("GET")
	//router.HandleFunc("/os/env/{id}", GetEnviron).Methods("GET")

	//TODO: Have a default self signed cert and key but pull cert & key from environment variables. Possibly cert/key names from vars and open files.
	cer, err := tls.X509KeyPair([]byte(certPem), []byte(keyPem))

	if err != nil {
		glog.Fatalln(err)
	}

	config := &tls.Config{Certificates: []tls.Certificate{cer}}
	ln, err := tls.Listen("tcp", fmt.Sprintf(":%d", kdebugAPIPort), config)
	if err != nil {
		glog.Fatalln(err)
	}

	glog.Infof("Application startup with API port %d and redir port %d at: %s\n", kdebugAPIPort, time.Now())
	//TODO: This should become:
	//glog.Infof("Application startup with API port %d and redir port %d at: %s\n", kdebugAPIPort, kdebugShellPort, time.Now())

	//TODO: We are wrapping this in a go func so that ServeDebug does not block. This means if their code doesn't have something preventing it from exiting it will exit. Do we want to find a way to detect that so you could only serve us? Probably not. Perhaps we just don't wrap this in a go func and they have to do it themselves just like we have to when we call http directly?
	go func() {
		glog.Fatal(http.Serve(ln, router))
	}()

}