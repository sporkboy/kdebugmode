package os

import (
	"os"
)

// Return an array of all environment variables
func Environ() ([]string) {
	return os.Environ()
}

// Return a specific environment variable.
func Getenv(variable string) (string) {
	return os.Getenv(variable)
}