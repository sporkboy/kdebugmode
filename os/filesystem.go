package os

import (
	"io/ioutil"
	"os"
	"time"
)

//TODO: Use a type that is less stupid. The mode on os.FileInfo is horrible. Possibly provided by https://github.com/spf13/afero
// We have to create our own type for fileinfo so that it can be serialized.
type FileInfo struct {
	Name    string
	Size    int64
	Mode    os.FileMode
	ModTime time.Time
	IsDir   bool
}

//TODO: Probably want to set some constraints on what can be listed.

// Fetch the names and details of all the files in the specified directory.
func ListFiles(path string) ([]FileInfo) {
	var files []FileInfo

	osFiles, err := ioutil.ReadDir(path)
	if err == nil {
		for _, osFile := range osFiles {
			newFile := FileInfo{
				Name:    osFile.Name(),
				Size:    osFile.Size(),
				Mode:    osFile.Mode(),
				ModTime: osFile.ModTime(),
				IsDir:   osFile.IsDir(),
			}
			files = append(files, newFile)
		}

	}
	return files
}