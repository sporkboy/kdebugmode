package os

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	. "gitlab.com/sporkboy/kdebugmode/debugserver"
	"fmt"
)

//TODO: Rename this package to
//TODO: We need to return more than endpoints. Each endpoint needs to tell us it's method. We may also need it to provide some sort of documentation in order to make swagger happen.


var thisModule = KDebugModule{
	Namespace:	"os",
	Handlers:	[]Handler{},
}

func init() {
	// Populate the handlers we will respond to
	addHandler(Handler{
		Path:			"environ",
		HandlerFunc: 	GetEnvirons,
		Method:	 		"GET",
	})
	addHandler(Handler{
		Path:			"environ/{id}",
		HandlerFunc: 	GetEnviron,
		Method:	 		"GET",
	})
	addHandler(Handler{
		Path:			"file",
		HandlerFunc: 	GetFiles,
		Method:	 		"GET",
	})
	addHandler(Handler{
		Path:			"file/{path}",
		HandlerFunc: 	GetFile,
		Method:	 		"GET",
	})
}


// This method is called by your code when you enable this module.
func RegisterMe() (KDebugModule) {
	return thisModule
}

// Add a handler to the list of what this module serves.
func addHandler(handler Handler) {
	thisModule.Handlers = append(thisModule.Handlers, handler)
}

func GetEnvirons(w http.ResponseWriter, r *http.Request) {
	varNames := Environ()

	json.NewEncoder(w).Encode(varNames)
}

func GetEnviron(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, _ := params["id"]

	json.NewEncoder(w).Encode(Getenv(id))
}

func GetFiles(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Query().Get("path")

	files := ListFiles(path)
	fmt.Printf("Got back files: %v", files)
	json.NewEncoder(w).Encode(files)
}

//TODO: Make this bit work.
func GetFile(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	path, _ := params["path"]

	json.NewEncoder(w).Encode(path)
}