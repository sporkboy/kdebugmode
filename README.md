# About
kdebugmode is a library intended to add troubelshooting functionality to go applications which are being delivered as containers.

When running apps in containers many people build their containers from an OS base image so that it has things like bash and ping or curl or whatever.

Which is annoying because the whole reason we run in containers is that we don't have to deal with the constraints of maintaining all this extra software.

Ideally our containers would contain:
* our code

And be done. 

O.K. a lot of code requires a few other things (like a run-time) so perhaps:
* our code
* the runtime
* a small set of REQUIRED files like our certificate trust store.

One of the main reasons for Golang's increasing popularity is the speed and ease at which it compiles to a native binary.
Many developers like myself find it perfectly suited for building into a scratch container which contains no outside code.

This can make it harder to troubleshoot certain things. How do I test DNS resolution for a service my code is calling to find out if the reason it isn't working is because I'm not doing it wrong or because the zone I am trying to resolve is unreachable from where the container is running?

So instead of breaking down and building a container with a full OS. let's build a library. A library that can be included in debug builds of the application and provide a way to perform these sorts of tests.

This library would listen on a different port than the primary application and accept API calls which would trigger certain tests (ping, port check, etc.)
I have seen things like this for other languages which provide their own shell within the application. I would rather provide this through an API.

This API would need to be able to be secured. Certificate based auth is probably the easiest but we will probably want to support kubelogin.

A client may need to be created to facilitate the execution of these  tests.

# Debug functions
Things that we may want to do when troubleshooting and which should be included.

## Kubernetes/container troubleshooting
* Get environment varialbes
* Verify role or something?
* Access files which are created for everyone (the pod's token for interacting with the API server) this is a kube specific thing but it does cross into the filesystem stuff below.

## Network troubleshooting
* Ping
* DNS resolution
* Retrieve arbitrary web content (curl)
* Network port check (netcat)
* Traceroute

## Process troubleshooting
* System (container/pod) memory
* System (container/pod) cpu
* Process list (for multi-container pods, are all my containers running?)

## Filesystem troubleshooting (for containers with filesystems)
* List files (ls)
* View the contents of a file (cat)
* Some sort of text file editor? Or the ability to copy files to/from?

## Code Tracing?
* Some sort of ability to generate a thing like a stack trace?

What else?

## Statistics
* Include prometheus metrics or just recommend it.
** We might want to expose some (or all?) of the debug functionality in prom format. This would probably require we research how to implement the client in a way that guarantees we don't conflict with whatever they may be doing normally.

# Priorities
Process troubleshooting information can be provided other ways (like by including the prometheus library and querying it's metrics) so the initial focus will be on some of the environment stuff and then probably the network troubleshooting tools.


# Notes on Libraries
## Kubernetes client-go
client.go currently includes almost all of kubernetes. So using that library would DRAMATICALLY increase the size of any code implementing this library.
The alternative library is here https://github.com/ericchiang/k8s no idea how stable/well supported it is.



# Architexture
Ultimately I would prefer to have this as modules. You include the base and get the web(and possibly crashshell) server.
Then if you include the network package you have https://<yourapp>:<your kdebugmoed port>/api/<version>/network functions
If you include the k8s module you have https://<yourapp>:<your kdebugmoed port>/api/<version>/k8s functions

This way you can choose which components to include and control how much we are impacting your code footprint.

This initial version will not work this way because I don't know how to make that happen (I have some thoughts) and I don't want to delay this.



# How to do the plugin things.

Within the server package write a register function. The register function takes an argument which is a function.

Each package has a function for registering itself and that's what you pass to the server register function.

